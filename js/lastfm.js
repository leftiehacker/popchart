var key = 'c257eb7b51a5d9e2d2c5c03a882e8d7e',
results = document.querySelector('.results'),
select = document.querySelector('.search-select'),
search = document.querySelector('.search')

var createProfile = {
  template (track) {
    var card = document.createElement('div')
    card.classList.add('flipInY', 'track')
    var image = '<img class="track-image" alt="' + track.artist.name + '" src="' + track.image[2]['#text'] + '">',
    artist = '<p class="track-artist">' + track.artist.name.toUpperCase() + '</p>',
    url = '<p class="track-url-container"><a class="track-url" target="_blank" href="' + track.artist.url + '">@' + track.artist.name + '</a></p>',
    name = '<p class="track-name">' + track.name + '</p>'
    card.innerHTML = image + artist + url + name
    return card
  },
  track (track) {
    var card = createProfile.template(track),
    rank = '<div class="track-rank-container">RANK<p class="track-rank">' + (+track['@attr'].rank + 1) + '</p></div>',
    listeners = '<div class="track-listeners-container">LISTENERS<p class="track-listeners">' + track.listeners + '</p></div>'
    card.insertAdjacentHTML('beforeend', listeners)
    card.insertAdjacentHTML('beforeend', rank)
    results.appendChild(card)
  },
  topTrack (track) {
    var card = createProfile.template(track),
    rank = '<div class="track-rank-container">RANK<p class="track-rank">' + track['@attr'].rank + '</p></div>',
    listeners = '<div class="track-listeners-container">LISTENERS<p class="track-listeners">' + track.listeners + '</p></div>'
    card.insertAdjacentHTML('beforeend', listeners)
    card.insertAdjacentHTML('beforeend', rank)
    results.appendChild(card)
  },
  tagTrack (track) {
    var card = createProfile.template(track),
    rank = '<div class="track-rank-container">RANK<p class="track-rank">' + track['@attr'].rank + '</p></div>',
    duration = '<tim class="track-listeners-container">DURATION<time class="track-listeners">'
    + Math.floor(track.duration/60) + ':'
    + (track.duration % 60 < 10 ? '0' + track.duration % 60 : track.duration % 60) + '</time></div>'
    card.insertAdjacentHTML('beforeend', duration)
    card.insertAdjacentHTML('beforeend', rank)
    results.appendChild(card)
  },
  error (msg) {
    var alert = document.createElement('h1')
    alert.classList.add('error-message')
    alert.innerHTML = 'No sign of "<i>' + msg + '</i> " here.'
    results.appendChild(alert)
  },
  prepare () {
    //reveal results of initial search
    if (results.innerHTML === '') {
      document.querySelector('header').classList.add('sidebar')
      results.style.display = 'block'
    }
    results.innerHTML = ''
  }
}


function getTracks() {
  var val = search.value,
  method = select.options[select.selectedIndex].value,
  url = 'https://ws.audioscrobbler.com/2.0/?method=' + method + val + '&api_key=' + key + '&format=json'

  //access Last.fm API
  fetch(url).then(response => {
    return response.json()
  }).then(data => {
    //route data to createProfile
    if (data.error) {
      createProfile.prepare()
      createProfile.error(val)
    } else {
      createProfile.prepare()
      switch (method)  {
        case 'geo.gettoptracks&country=':
        data.tracks.track.forEach(createProfile.track)
        break
        case 'artist.gettoptracks&artist=':
        data.toptracks.track.forEach(createProfile.topTrack)
        break
        case 'tag.gettoptracks&tag=' :
        if (data.tracks.track.length === 0) createProfile.error(val)
        else data.tracks.track.forEach(createProfile.tagTrack)
        break
      }
    }
  }).then(() => {
    var iso = new Isotope(results, {
      itemSelector: '.track',
      stagger: 80
    })
  })

}

//trigger search on CLICK and on ENTER
document.querySelector('.search-btn').addEventListener('click', getTracks)
search.addEventListener('keypress', e => {
  if (e.keyCode === 13) {
    getTracks()
  }
});
